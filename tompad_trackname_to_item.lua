--  --[[
--  * ReaScript Name: tompad_trackname_to_item.lua
--  * Description: Rename items from track names
--  * Author: tompad
--  * Repository: Bitbucket
--  * Repository URI: https://bitbucket.org/TompaD/
--  * Licence: GPL v3
--  * REAPER: 5.0
--  * Version: 1.0
-- --]]

-- -- Display a message in the console for debugging
-- function Msg(value)
--   if console then
--     reaper.ShowConsoleMsg(tostring(value) .. "\n")
--   end
-- end


reaper.PreventUIRefresh(1)


reaper.Undo_BeginBlock()

if reaper.CountSelectedMediaItems(0) > 0 then
  for i = 0, reaper.CountSelectedMediaItems(0) - 1 do
    local take = reaper.GetActiveTake( reaper.GetSelectedMediaItem(0, i) )
    if take then
      local track = reaper.GetMediaItemTake_Track( take )
      local retval, clipboard = reaper.GetTrackName(track, "")
      reaper.GetSetMediaItemTakeInfo_String(take, "P_NAME", clipboard, true)
    end
  end
end

reaper.Undo_EndBlock("Rename selected takes from tracks", - 1)

reaper.PreventUIRefresh(-1)
