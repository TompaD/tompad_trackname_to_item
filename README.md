# tompad_trackname_to_item

tompad_trackname_to_item is a Reascript (for Reaper DAW) for naming items with the tracks names.
___

Any comments on coding, requests, bugs etc is welcome! PM me (tompad) on Reaper Forum (https://forum.cockos.com/member.php?u=19103).

This script wouldn't exist without the help from the Reaper community:

- Lokasenna
- XRayM
- cfillion
- mespotine

... and many more, **thanks to you all!**
